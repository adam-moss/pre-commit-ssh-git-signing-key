# Pre-Commit-SSH-Git-Signing-Key

[![Release][repo-current-release-image]][repo-url]
[![License][license-image]][license-url]

Some out-of-the-box hooks for [pre-commit] that I find useful or meet specific needs not catered for elsewhere

## Using Pre-Commit-SSH-Git-Signing-Key with Pre-Commit

Add this to your `.pre-commit-config.yaml`

```yaml
- repo: https://gitlab.com/adam-moss/pre-commit-ssh-git-signing-key
  rev: v1.0.0 # Use the ref you want to point at
  hooks:
    - id: add-ssh-git-signing-key
    - id: remove-ssh-git-signing-key
  # -   id: ...
```

## Hooks Available

### `add-ssh-git-signing-key`

![commit-msg hook][hook-commit-msg-image]

Add the configured `user.signingKey` to `ssh-agent`

- checks `user.signingKey` is an `ssh` signing key
- add the key to `ssh-agent` with no timeout, prompting for the passphrase if set
- if running on MacOS includes the option `--apple-use-keychain`

### `remove-ssh-git-signing-key`

![post-commit hook][hook-post-commit-image]

Removes the configured `user.signingKey` from `ssh-agent`

[hook-commit-msg-image]: https://img.shields.io/badge/hook-commit--msg-informational?logo=git
[hook-post-commit-image]: https://img.shields.io/badge/hook-post--commit-informational?logo=git
[license-image]: https://img.shields.io/gitlab/license/adam-moss/pre-commit-ssh-git-signing-key
[license-url]: https://opensource.org/licenses/ISC
[pre-commit]: https://www.pre-commit.com
[repo-current-release-image]: https://img.shields.io/gitlab/v/tag/adam-moss/pre-commit-ssh-git-signing-key?include_prereleases&sort=semver
[repo-url]: https://gitlab.com/adam-moss/pre-commit-ssh-git-signing-key
